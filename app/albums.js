const express = require("express");
const multer = require("multer");
const nanoid = require("nanoid");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const path = require("path");
const Album = require("../models/Album");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const router = express.Router();

const createRouter = () => {

  router.get("/", (req, res) => {
    const artistId = req.query.artist;
    if (artistId) {
      Album.find({ author: artistId })
      .populate("author")
      .then(
        result => (result ? res.send(result) : res.sendStatus(404))
      );
    } else {
      Album.find({publish: true})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }
  });

  router.get('/forAdmin', [auth, permit('admin')], async(req, res) =>{
    try { 
      const results = await Album.find().sort({publish: 1});
      if (results) res.send(results);
    } catch (error) {
      res.status(500).send({error: error});
    }
  });

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    Album.find({userId: id})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  router.post("/", [auth, upload.single("photo")], (req, res) => {
    const albumData = req.body;

    if (req.file) {
      albumData.photo = req.file.filename;
    } else {
      albumData.photo = null;
    }

    const album = new Album(albumData);
    album
      .save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;
