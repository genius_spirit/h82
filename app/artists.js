const express = require("express");
const multer = require("multer");
const nanoid = require("nanoid");
const path = require("path");
const Artist = require("../models/Artist");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const router = express.Router();

const createRouter = () => {
  router.get("/", (req, res) => {
    Artist.find({publish: true})
      .sort({name: 1})
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.get('/admin', [auth, permit('admin')], async(req, res) =>{
      try {
        const results = await Artist.find().sort({publish: 1});
        if (results) res.send(results);
      } catch (e) {
        res.status(500).send({error: e});
      }
  });

  router.get("/:id", auth, async(req, res) => {
    try {
      const id = req.params.id;
      const result = await Artist.find({ userId: id });
      if (result) res.send(result);
      else res.sendStatus(404);
    } catch(e) {
      res.status(500).send({error: e});
    }
  });

  router.post("/", [auth, upload.single("photo")], (req, res) => {
    const artistData = {
      name: req.body.name,
      info: req.body.info || '',
      userId: req.body.userId
    };

    if (req.file) {
      artistData.photo = req.file.filename;
    } else {
      res.status(400).send({message: "Photo must be set!"});
    }

    const artist = new Artist(artistData);
    artist
      .save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send({error: error}));
  });

  return router;
};

module.exports = createRouter;
