const express = require("express");
const multer = require("multer");
const nanoid = require("nanoid");
const auth = require('../middleware/auth');
const config = require("../config");
const path = require("path");
const Track = require("../models/Track");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.musicPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const createRouter = () => {
  router.get("/", (req, res) => {
    const albumId = req.query.album;
    if (albumId) {
      Track.find({ album: albumId, publish: true })
      .populate('album')
      .sort({position: 1}).then(
        result => (result ? res.send(result) : res.sendStatus(404))
      );
    } else {
      Track.find({publish: true})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }
  });

  router.post("/", [auth, upload.single("file")], (req, res) => {
    const trackData = req.body;

    if (req.file) {
      trackData.file = req.file.filename;
    } else {
      trackData.file = null;
    }

    const track = new Track(trackData);
    track
      .save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;
