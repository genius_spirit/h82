const express = require("express");
const auth = require('../middleware/auth');
const TracksHistory = require("../models/TracksHistory");
const router = express.Router();

const createRouter = () => {

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    TracksHistory.find({user: id})
    .sort({datetime: -1})
    .populate({path: 'track', populate: {path: 'album', populate: {path: 'author'}}})
    .then(result => {
      if (result) res.send(result);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
  });


  router.post("/", auth, async (req, res) => {
  const tracksHistory = new TracksHistory({
    user: req.user._id,
    track: req.body.track
  });
  tracksHistory
    .save()
    .then(result => res.send(result))
    .catch(error => res.sendStatus(500).send(error));
  });

  return router;
};

module.exports = createRouter;
