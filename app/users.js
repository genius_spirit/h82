const express = require("express");
const User = require("../models/User");
const router = express.Router();

const createRouter = () => {

  router.post("/", (req, res) => {
    const user = new User({
      username: req.body.username,
      password: req.body.password
    });
    user
    .save()
    .then(user => res.send(user))
    .catch(error => res.status(400).send({error: error}));
  });

  router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(400).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(400).send({error: 'Password is wrong!'});
    }

    user.generateToken();
    await user.save();

    return res.send({message: 'Username and password correct!', user});
  });

  router.delete('/sessions', async(req, res) => {
    const token = req.get('Token');
    const success = {message: 'Success'};
    if (!token) return res.send(success);

    const user = await User.findOne({token});
    if (!user) res.send(success);

    user.generateToken();
    await user.save();
    return res.send(success);
  });

  return router;
};

module.exports = createRouter;
