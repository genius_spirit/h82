const mongoose = require('mongoose');
const config = require("./config");
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

const collections = ['artists', 'albums', 'tracks', 'users'];

db.once("open", async () => {

  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

   const [user, admin] = await User.create({
     username: 'user',
     password: 'user123',
     role: 'user'
   }, {
     username: 'admin',
     password: 'admin123',
     role: 'admin'
   });

  const [Basta, Roxette] = await Artist.create({
    name: 'Баста',
    photo: 'basta.jpg',
    info: 'Васи́лий Миха́йлович Вакуле́нко — российский музыкант (исполнитель рэпа и других жанров, битмейкер, композитор). Известен под творческими псевдонимами и проектами Ба́ста, Ногга́но, N1NT3ND0 (Нинтéндо); ранее — Баста Хрю, Баста Басти́ллио. Сооснователь и участник коллектива Bratia Stereo. С 2007 года — совладелец лейбла «Gazgolder»'
  }, {
    name: 'Roxette',
    photo: 'roxette.jpg',
    info: 'Roxette (произносится «роксэ́т») — шведская поп-рок-группа, лидерами которой являются Пер Гессле (автор текстов песен и музыки, гитара, вокал, губная гармоника) и Мари Фредрикссон (вокал, рояль). Как и многие другие шведские музыканты, они исполняют свои песни на английском языке. Само название «Roxette» происходит от названия песни группы Dr. Feelgood.'
  });

  const [basta4, bastaGuf, tourism, look] = await Album.create({
    name: 'Баста 4',
    year: '2013',
    about: 'Альбом был целиком записан на личной студии Басты в здании Gazgolder. В результате было подготовлено 22 трека, из которых было решено оставить 11. Самый старый трек с альбома был сочинен ещё в 2007 году. В альбом вошли и те треки, которые изначально планировались для проектов Bratia Stereo и N1NT3ND0. В итоге в список композиций альбома были включены 17 треков.',
    author: Basta._id,
    photo: 'basta4.jpg',
    publish: true,
    userId: user._id
  }, {
    name: 'Баста/Гуф',
    year: '2010',
    about: '«Баста/Гуф» — совместный альбом Басты и Gufа, вышедший в ноябре 2010 года. Результатом их совместной работы в этом году стала пластинка фактически без названия, в сером буклете.',
    author: Basta._id,
    photo: 'basta-guf.jpg',
    publish: false,
    userId: admin._id
  }, {
    name: 'Tourism',
    year: '1992',
    about: 'Диск был записан во время их всемирного турне Join the Joyride Tour и является смесью живых и студийных треков. Согласно легенде, некоторые композиции альбома даже были записаны в гостиничных номерах, причем вместо ударных установок использовались гастрольные чемоданы группы.',
    author: Roxette._id,
    photo: 'tourism.jpg',
    publish: true,
    userId: user._id
  }, {
    name: 'Look Sharp!',
    year: '1988',
    about: 'На обложке изображены Мари и Пер, прорывающиеся через толпу поклонников, для того, чтобы сесть в чёрный лимузин марки Cadilac. Чтобы популярный дуэт сел в машину, полиции потребовалось 25 минут для «расчистки» пути. В Германии альбом был выпущен ограниченным тиражом на пластинке с картинкой (picture LP) и на CD с картинкой (picture CD). Эти издания — первые диски с картинками от Roxette',
    author: Roxette._id,
    photo: 'look.jpg',
    publish: false,
    userId: admin._id

  });

  await Track.create({
    position: 3,
    name: 'Мама',
    album: basta4._id,
    duration: '4.35',
    file: '',
    youtube: '_-pIVd4lGMw',
    userId: user._id,
    publish: true
  }, {
    position: 4,
    name: 'Листья пуэра под кипяток',
    album: basta4._id,
    duration: '4.00',
    file: '',
    youtube: '',
    userId: admin._id,
    publish: true
  }, {
    position: 9,
    name: 'Crazymflove',
    album: basta4._id,
    duration: '4.59',
    file: '',
    youtube: 'kwS02GzwK-U',
    userId: user._id
  }, {
    position: 15,
    name: 'Одна любовь',
    album: basta4._id,
    duration: '5.27',
    file: '',
    youtube: 'SJlmYRZmgXY',
    userId: user._id
  }, {
    position: 1,
    name: 'Другая волна',
    album: bastaGuf._id,
    duration: '5.03',
    file: '',
    youtube: '3-OWpZofnXk',
    userId: user._id,
    publish: true
  }, {
    position: 6,
    name: 'Китай',
    album: bastaGuf._id,
    duration: '4.41',
    file: '',
    youtube: '-3eZANHMh7w',
    userId: admin._id,
    publish: true
  }, {
    position: 8,
    name: 'Заколоченное',
    album: bastaGuf._id,
    duration: '4.09',
    file: '',
    youtube: 'NQufQSpMUic',
    userId: user._id
  }, {
    position: 14,
    name: 'Личное дело',
    album: bastaGuf._id,
    duration: '3.49',
    file: '',
    youtube: '',
    userId: admin._id
  }, {
    position: 1,
    name: 'How do you do!',
    album: tourism._id,
    duration: '3.11',
    file: '',
    youtube: 'imW5F1OO9N4',
    userId: user._id,
    publish: true
  }, {
    position: 7,
    name: 'It Must Have Been Love',
    album: tourism._id,
    duration: '7.05',
    file: '',
    youtube: 'k2C5TjS2sh4',
    userId: admin._id,
    publish: true
  }, {
    position: 10,
    name: 'Silver Blue',
    album: tourism._id,
    duration: '4.07',
    file: '',
    youtube: '',
    userId: user._id
  }, {
    position: 15,
    name: 'Joyride',
    album: tourism  ._id,
    duration: '4.48',
    file: '',
    youtube: 'xCorJG9mubk',
    userId: admin._id
  },  {
    position: 1,
    name: 'The look',
    album: look._id,
    duration: '3.57',
    file: '',
    youtube: 'LlVI7ZNiFlI',
    userId: user._id,
    publish: true
  }, {
    position: 13,
    name: 'Listen to Your Heart',
    album: look._id,
    duration: '5.30',
    file: '',
    youtube: 'yCC_b5WHLX0',
    userId: admin._id,
    publish: true
  });

  db.close();
});