const User = require('../models/User');

const auth = (req, res, next) => {
  const token = req.get('Token');
  if (!token) {
    return req.status(401).send({error: 'Token not provided!'});
  }

  User.findOne({token: token}).then(user => {
    if (!user) {
      return res.sendStatus(401).send({error: 'User not found'});
    }

    req.user = user;

    next();
  })
};

module.exports = auth;
