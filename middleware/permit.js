const permit = (...roles) => {
  return (req, res, next) => {
    if (!req.user) {
      return res.status(401).send({ message: "You are not authenticated" });
    }
    if (!roles.includes(req.user.role)) {
      return res.status(403).send({ message: "You do not have access rights" });
    }
    next();
  };
};

module.exports = permit;
