const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  year: {
    type: Number,
    min: 1900,
    required: true
  },
  about: String,
  author: {
    type: Schema.Types.ObjectId,
    ref: "Artist",
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  publish: {
    type: Boolean,
    default: false
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  }
});

const Album = mongoose.model("Album", AlbumSchema);

module.exports = Album;
