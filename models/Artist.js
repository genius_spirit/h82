const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  info: {
    type: String,
  },
  publish: {
    type: Boolean,
    default: false
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  }
});

const Artist = mongoose.model("Artist", ArtistSchema);

module.exports = Artist;
