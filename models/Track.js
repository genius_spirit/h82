const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  position: {
    type: Number,
    required: true,
    min: 0,
    max: 25
  },
  name: {
    type: String,
    required: true,
    unique: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: "Album",
    required: true
  },
  duration: {
    type: String,
    required: true
  },
  file: String,
  youtube: String,
  publish: {
    type: Boolean,
    default: false
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  }
});

const Track = mongoose.model("Track", TrackSchema);

module.exports = Track;
