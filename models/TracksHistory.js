const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const TracksHistorySchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: true
  },
  track: {
    type: Schema.Types.ObjectId,
    ref: "Track",
    required: true
  },
  datetime: {
    type: Date,
    default: moment({})
  }
});

const TracksHistory = mongoose.model('TracksHistory', TracksHistorySchema);
module.exports = TracksHistory;