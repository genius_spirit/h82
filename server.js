const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config");

const artists = require("./app/artists");
const albums = require("./app/albums");
const tracks = require("./app/tracks");
const users = require('./app/users');
const tracksHistory = require('./app/tracksHistory');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

db.once("open", () => {
  console.log("Mongoose connected!");

  app.use("/artists", artists());
  app.use("/albums", albums());
  app.use("/tracks", tracks());
  app.use('/users', users());
  app.use('/tracks_history', tracksHistory());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
